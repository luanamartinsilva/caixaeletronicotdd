import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CaixaEletronicoTest {

	private ContaCorrente conta;
	private ServicoRemotoMock mock;
	private CaixaEletronico caixaEletronico;
	private HardwareMock hardwareMock;
	
	@Before
	public void configurandoDadosIniciais() {

		conta = new ContaCorrente();
		conta.saldo = 50.0;
		conta.numero = "12345";

		mock = new ServicoRemotoMock();
		mock.setContaRecuperada(conta);

		caixaEletronico = new CaixaEletronico(conta.numero, mock);
		hardwareMock = new HardwareMock();

	}

	@Test
	public void testarSaldo() {

		assertEquals("O saldo é R$50,00", caixaEletronico.saldo());

	}

	@Test
	public void testarDepositoSucesso() {

		assertEquals("Deposito recebido com sucesso", caixaEletronico.depositar(50.0));
		assertEquals(new ContaCorrente(100.0, conta.numero), mock.getContaPersisitida());

	}

	@Test
	public void testarDepositoFalha() {

		mock.setExcecaoPersisitirConta(new RuntimeException());

		assertEquals("Falha ao realizar depósito", caixaEletronico.depositar(50.0));
		assertEquals(null, mock.getContaPersisitida());

	}

	@Test
	public void testarSacarSucesso() {

		assertEquals("Retire seu dinheiro", caixaEletronico.sacar(10.0));
		assertEquals(new ContaCorrente(40.0, conta.numero), mock.getContaPersisitida());
		assertEquals("entregarDinheiro", hardwareMock.servicoChamado());

	}
	
	@Test
	public void testarSacarSaldoInsuficiente() {

		assertEquals("Saldo insuficiente", caixaEletronico.sacar(60.0));
		assertEquals(null, mock.getContaPersisitida());

	}

}
