import java.util.Locale;

public class CaixaEletronico {

	private String numeroContaCorrente;
	private ServicoRemoto servicoRemoto;

	public CaixaEletronico(String numeroConta, ServicoRemoto servicoRemoto) {
		this.servicoRemoto = servicoRemoto;
		this.numeroContaCorrente = numeroConta;
	}

	public String saldo() {
		ContaCorrente contaCorrente = servicoRemoto.recuperarConta(this.numeroContaCorrente);
		String messageTemplate = "O saldo é R$%02.2f";
		return String.format(Locale.FRANCE, messageTemplate, contaCorrente.saldo);
	}

	public String depositar(double valoDeposito) {

		ContaCorrente contaCorrente = servicoRemoto.recuperarConta(this.numeroContaCorrente);
		contaCorrente.saldo += valoDeposito;

		try {
			servicoRemoto.persistirConta(contaCorrente);
			return "Deposito recebido com sucesso";
		} catch (Exception e) {
			return "Falha ao realizar depósito";
		}

	}

	public String sacar(double valorSaque) {
		ContaCorrente contaCorrente = servicoRemoto.recuperarConta(this.numeroContaCorrente);
		if (valorSaque < contaCorrente.saldo) {
			contaCorrente.saldo -= valorSaque;
			servicoRemoto.persistirConta(contaCorrente);
			return "Retire seu dinheiro";
		} else {
			return "Saldo insuficiente";
		}

	}

}
