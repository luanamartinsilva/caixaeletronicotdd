
public class ServicoRemotoMock implements ServicoRemoto {
	
	private ContaCorrente contaRecuperada;
	private ContaCorrente contaPersisitida;
	private RuntimeException excecaoPersisitirConta;

	@Override
	public void persistirConta(ContaCorrente conta) throws RuntimeException {
		if (this.excecaoPersisitirConta != null) {
			throw this.excecaoPersisitirConta;
		}
		this.contaPersisitida = conta;		
	}

	@Override
	public ContaCorrente recuperarConta(String numero) {
		return contaRecuperada;
	}
	
	/* metodos para teste */
	
	public void setContaRecuperada(ContaCorrente contaRecuperada) {
		this.contaRecuperada = contaRecuperada;
	}

	public void setExcecaoPersisitirConta(RuntimeException excecaoPersisitirConta) {
		this.excecaoPersisitirConta = excecaoPersisitirConta;
	}

	public ContaCorrente getContaPersisitida() {
		return contaPersisitida;
	}


}
