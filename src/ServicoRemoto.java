
public interface ServicoRemoto {

	public void persistirConta(ContaCorrente conta) throws RuntimeException;

	public ContaCorrente recuperarConta(String numero);

}
