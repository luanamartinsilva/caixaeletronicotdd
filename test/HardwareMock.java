
public class HardwareMock implements Hardware {
	
	private String servicoChamado;

	public String servicoChamado() {
		return this.servicoChamado;
	}

	@Override
	public void entregarDinheiro() {
		this.servicoChamado = "entregarDinheiro";
		
	}

}
